import os
import pickle as pkl
import numpy as np
from cottonwood.core.blocks.activation import Logistic, TanH
from cottonwood.core.blocks.conv1d import Conv1D
from cottonwood.core.blocks.linear import Linear
from cottonwood.core.blocks.loss import MeanSquareLoss
from cottonwood.core.blocks.operations import \
    Copy, Difference, Flatten, HardMax, OneHot
from cottonwood.core.blocks.pooling import MaxPool1D
from cottonwood.core.blocks.structure import Structure
import cottonwood.core.toolbox as tb
import cottonwood.examples.convnet.conv1d_viz as conv_viz
import cottonwood.examples.simulation.visualize_structure as struct_viz
from cottonwood.core.logger import ConfusionLogger
from cottonwood.core.optimizers import Momentum
from data import TrainingData, TuningData, TestingData


N_EVAL_RUNS = 5
N_TRAINING_ITER = int(2e5)
N_TUNING_ITER = int(2e4)
N_TESTING_ITER = int(2e4)
N_REPORT_INTERVAL = int(1e4)
N_VIZ_INTERVAL = int(1e6)
VERBOSE = True



def main():
    """
    For the final training, we're no longer interested in getting the
    most representative value. Now we want to juice the architecture for
    the absolute best set of learned weights we can get. There's a good
    amount of luck involved in this, so we're going to make a lot of attempts.
    And keep the best. This is a one-time operation, so it's OK
    for this to take a while.
    """
    reports_dir = os.path.join("reports", tb.date_string())
    # Train the model repeatedly.
    # Keep the best one with the best performance on the tuning data.
    tuning_losses = []
    lowest_so_far = 1e10
    best_model = None
    for i_run in range(N_EVAL_RUNS):
        print(f"Run {i_run + 1} of {N_EVAL_RUNS}")
        classifier, loss = train(
            reports_dir=reports_dir,
            verbose=VERBOSE)
        print(loss)
        tuning_losses.append(loss)
        if loss < lowest_so_far:
            lowest_so_far = loss
            best_model = classifier

    # Evaluate the model on the testing data to get a more realistic
    # assessment of it's performance on unseen data.
    testing_loss = evaluate(
        best_model,
        reports_dir=reports_dir,
        verbose=VERBOSE,
    )

    filename = "heartbeat_classifier.pkl"
    print("All done!")
    print(f"Model saved at {filename}")
    print(f"The best model had a tuning loss of {lowest_so_far}")
    print(f"and a testing loss of {testing_loss}.")
    print("The tuning losses had a 10th, 25th, median, 75th, 90th, and max:")
    print(
        f"{np.percentile(tuning_losses, 10):.4f}"
        + f", {np.percentile(tuning_losses, 25):.4f}"
        + f", {np.percentile(tuning_losses, 50):.4f}"
        + f", {np.percentile(tuning_losses, 75):.4f}"
        + f", {np.percentile(tuning_losses, 90):.4f}"
        + f", {np.max(tuning_losses):.4f}"
    )

    # Save the model out for future use
    with open(filename, "wb") as f:
        pkl.dump(classifier, f)


def train(
    ConvActivation=TanH,
    kernel_size=3,
    learning_rate=3e-3,
    n_kernels=16,
    reports_dir="reports",
    verbose=False,
    **kwargs,
):
    if verbose:
        os.makedirs(reports_dir, exist_ok=True)

        msg = f"""

    Training convolutional neural network on the ECG data set.
    Look for documentation and visualizations
    in the {reports_dir} directory.

    """
        print(msg)

    classifier = Structure()
    classifier.add(TrainingData(), "training_data")

    # Add blocks for the classification branch of the network
    classifier.add(Conv1D(
        kernel_size=kernel_size,
        n_kernels=n_kernels,
        optimizer=Momentum(learning_rate=learning_rate),
    ), "convolution_0")
    classifier.add(ConvActivation(), "activation_0")
    classifier.add(MaxPool1D(), "max_pool_0")

    classifier.add(Conv1D(
        kernel_size=kernel_size,
        n_kernels=n_kernels,
        optimizer=Momentum(learning_rate=learning_rate),
    ), "convolution_1")
    classifier.add(ConvActivation(), "activation_1")
    classifier.add(MaxPool1D(), "max_pool_1")

    classifier.add(Flatten(), "flatten")
    classifier.add(Linear(
        32,
        optimizer=Momentum(learning_rate=learning_rate),
    ), "linear_0")
    classifier.add(ConvActivation(), "activation_lin_0")

    classifier.add(Linear(
        4,
        optimizer=Momentum(learning_rate=learning_rate),
    ), "linear_1")
    classifier.add(Logistic(), "logistic")

    classifier.add(Copy(), "logistic_copy")
    classifier.add(HardMax(), "hard_max")

    # Add blocks for the ground truth comparison branch
    classifier.add(OneHot(4), "one_hot")
    classifier.add(Difference(), "difference")
    classifier.add(MeanSquareLoss(), "mean_sq_loss")

    # Create all the connections between the blocks
    classifier.connect("training_data", "convolution_0", i_port_tail=0)
    classifier.connect("training_data", "one_hot", i_port_tail=1)

    classifier.connect("convolution_0", "activation_0")
    classifier.connect("activation_0", "max_pool_0")

    classifier.connect("max_pool_0", "convolution_1")
    classifier.connect("convolution_1", "activation_1")
    classifier.connect("activation_1", "max_pool_1")

    classifier.connect("max_pool_1", "flatten")
    classifier.connect("flatten", "linear_0")
    classifier.connect("linear_0", "activation_lin_0")

    classifier.connect("activation_lin_0", "linear_1")
    classifier.connect("linear_1", "logistic")
    classifier.connect("logistic", "logistic_copy")

    classifier.connect(
        "logistic_copy", "difference", i_port_tail=0, i_port_head=0)
    classifier.connect("logistic_copy", "hard_max", i_port_tail=1)
    classifier.connect("one_hot", "difference", i_port_head=1)
    classifier.connect("difference", "mean_sq_loss")

    # Execute the training loop
    for i_iter in range(N_TRAINING_ITER):
        classifier.forward_pass()
        classifier.backward_pass()
        if verbose:
            if (i_iter + 1) % N_VIZ_INTERVAL == 0:
                conv_viz.render(
                    classifier.blocks["convolution_0"],
                    reports_dir,
                    f"conv_0_{i_iter + 1:07}.png")
                conv_viz.render(
                    classifier.blocks["convolution_1"],
                    reports_dir,
                    f"conv_1_{i_iter + 1:07}.png")
    if verbose:
        tb.summarize(classifier, reports_dir=reports_dir)
        struct_viz.render(classifier, reports_dir)

    classifier.remove("training_data")
    classifier.add(TuningData(), "tuning_data")
    classifier.connect("tuning_data", "convolution_0", i_port_tail=0)
    classifier.connect("tuning_data", "one_hot", i_port_tail=1)

    confusion_logger = ConfusionLogger(
        reports_path=reports_dir,
        verbose=verbose,
    )
    # Execute the loop evaluating performance on the tuning data
    for i_iter in range(N_TUNING_ITER):
        classifier.forward_pass()
        confusion_logger.log_values(
            classifier.blocks["hard_max"].result,
            classifier.blocks["one_hot"].result,
            classifier.blocks["one_hot"].get_labels())
    classifier.remove("tuning_data")

    confusion_matrix_stats = confusion_logger.calculate_stats()
    mean_recall = np.mean(confusion_matrix_stats[:, 3])
    tuning_loss = 1 - mean_recall
    return classifier, tuning_loss


def evaluate(
    classifier,
    reports_dir="reports",
    verbose=False,
):
    """
    This is the final step.
    Evaluate the best-of-tuning model on the training set to get
    an honest measure of how well it performs.
    """
    classifier.add(TestingData(), "testing_data")
    classifier.connect("testing_data", "convolution_0", i_port_tail=0)
    classifier.connect("testing_data", "one_hot", i_port_tail=1)

    confusion_logger = ConfusionLogger(
        reports_path=reports_dir,
        verbose=verbose,
    )
    # Execute the loop evaluating performance on the tuning data
    for i_iter in range(N_TESTING_ITER):
        classifier.forward_pass()
        confusion_logger.log_values(
            classifier.blocks["hard_max"].result,
            classifier.blocks["one_hot"].result,
            classifier.blocks["one_hot"].get_labels())
    classifier.remove("testing_data")

    confusion_matrix_stats = confusion_logger.calculate_stats()
    mean_recall = np.mean(confusion_matrix_stats[:, 3])
    testing_loss = 1 - mean_recall
    return testing_loss


if __name__ == "__main__":
    main()
