# Case Study: Detecting anomalous heartbeats in ECG

This case study uses a convolutional neural network to classify
heartbeats in electrocardiogram (ECG) data.

Using two channels of ECG input with each 
beat labeled as normal, paced, premature ventricular contraction (PVC), or
left bundle branch block (LBBB),
this study learns to classify heartbeats. The final
result is a display of the raw ECG data with a color bar identifying
likely PVCs and LBBBs, indicators of a possible cardiac condition.

[![Thumbnail of results video](/images/results_video.png)](https://youtu.be/0lEUuNvgpDE)
[Video summary of the results](https://youtu.be/0lEUuNvgpDE)

If at any point you'd like to understand the motivation or conceptual
underpinnings of anything in this study, checkout
[End-to-End Machine Learning Course 321](https://e2eml.school/321)
in which this ECG classification problem is handled in fine detail.

## Get it running on your machine

This study is designed to run entirely on a desktop or laptop.
It doesn't use GPUs or clusters of CPUs.
You'll need to have Python 3 (including pip) installed.
There are a couple of other packages
you'll need to have in place to make it work.

### Cottonwood

This study builds a model using [Cottonwood](https://e2eml.school/cottonwood),
a lightweight machine learning framework. 
Cottonwood changes quickly, so make sure
you have the right version. This study was built with version 24.
To do this at the command line:

    git clone https://gitlab.com/brohrer/cottonwood.git
    python3 -m pip install -e cottonwood
    cd cottonwood
    git checkout v24
    cd ..

Installing Cottonwood will also install its dependencies if you don't have
them already, [Numpy](https://numpy.org/),
[Numba](https://numba.pydata.org/), and
[Matplotlib](https://matplotlib.org/).

### Ponderosa

If you plan to do any re-training and hyperparameter adjustment, you'll
need [Ponderosa](https://gitlab.com/brohrer/ponderosa),
a hyperoparameter optimization library created in
[Course 314](https://e2eml.school/314). To get it at the command line:

    git clone https://gitlab.com/brohrer/ponderosa.git
    python3 -m pip install -e ponderosa


### The Waveform Database

[The Waveform Database](https://github.com/MIT-LCP/wfdb-python)
(wfdb) is a library specifically
tailored to physiological data
of the format we'll be working with.

    python3 -m pip install wfdb


### Download this study

Finally, it's time to clone and install this repository.

    git clone https://gitlab.com/brohrer/study-ecg-rhythms.git
    python3 -m pip install -e study-ecg-rhythms


### Run the demo

This sequence will run the demo using the
pre-trained model and re-create the full set
of demo images showing five-second segments like the image above.
This may take the better part of an hour to run.

    cd study-ecg-rhythms
    python3 demo.py


### Re-train the model

If you'd like to get adventurous you can re-train the model from scratch.
You can probably improve on the model and get even better results.
To modify the model, make edits to `testing.py`.

    python3 testing.py

To try out new
architectures and hyperparameters, make edits to `tuning.py` and
run it first.

    python3 tuning.py

For hints on how to make and test changes, checkout out
[Course 321](https://e2eml.school/321).


## The data

This study makes heavy use of
[the MIT-BIH Arrhythmia Database](https://physionet.org/content/mitdb/1.0.0/)
(Moody GB, Mark RG. The impact of the MIT-BIH Arrhythmia Database.
IEEE Eng in Med and Biol 20(3):45-50 (May-June 2001). (PMID: 11446209)),
which is hosted by PhysioNet (
Goldberger, A., Amaral, L., Glass, L., Hausdorff, J., Ivanov, P. C.,
Mark, R., ... & Stanley, H. E. (2000). PhysioBank, PhysioToolkit, and
PhysioNet: Components of a new research resource for complex
physiologic signals. Circulation [Online]. 101 (23), pp. e215–e220.)
and made available under the
[Open Data Commons Attribution License v1.0](
https://physionet.org/content/mitdb/view-license/1.0.0/).
(Here is a [plain language summary](
https://www.opendatacommons.org/licenses/by/summary/index.html)).

I've downloaded the data to a directory called "data" and unzipped it there.
The file
[mit-bih-arrhythmia-database-1.0.0/mitdbdir/intro.html
](/mit-bih-arrhythmia-database-1.0.0/mitdbdir/intro.html)
is as thorough and clear a data description as I've ever seen.
This dataset has been lovingly cared for.

[The Wikipedia ECG page](https://en.wikipedia.org/wiki/Electrocardiography)
really helps
to make sense out of the data. It gives just enough domain knowledge
to avoid doing anything really dumb here.

For clarity, this study focuses on a subset of
the recordings that are most consistent and a subset of the heartbeat
labels that are most prevalent.
Our analyses can be extended later to include a wider set of recordings
and categories.

* Focus on 100-series recordings (not especially selected
      for pathological conditions).
* Focus on MLII (modified limb lead II) and V1 signal only.
* Ignore recordings 102, 104 (MLII not available)
* Ignore recording 114 (leads reversed)
* Ignore recordings 112, 115 through 124 (recorded at twice real time)

This leaves us with recordings 100, 101, 103, 105, 106, 107, 108, 109, 111, 113.

Class balance matters. We'll shoot for the roughly same number
of training examples from each class. These four classes are most
prevalent.

* 'N': 13,742 instances. Normal
* 'L':  4,615 instances. Left bundle branch block
* '/':  2,078 instances. Paced beat
* 'V':    677 instances. Premature ventricular contraction

We can do some oversampling of smaller classes, but too much oversampling
means we're just overfitting to a handful of examples. For now we'll
limit ourselves to learning 'N', 'V', '/', and 'L'.
We can always extend it to other classes later.

## The model

A trained copy of the model is included in the repository
as heartbeat_classifier.pkl.
[Here is an example](https://gitlab.com/brohrer/study-ecg-rhythms/-/blob/master/demo.py#L23-25)
of how to load it up.
The classifier used here is the convolutional neural network pictured below. 

![Model block diagram](/images/structure_diagram.png)

It has two convolutional layers and two linear layers. Most of the
activation functions are hyperbolic tangents, except for the final one,
which is a logistic function. For a detailed report of the parameters
and settings of the network, check out
[this summary](/reports/structure_summary.txt).

The model performance is captured in the confusion matrix.

![Confusion matrix](images/confusion_matrix.png)

The model was optimized to have the best average recall across all
classes. Classes were weighted equally. 
