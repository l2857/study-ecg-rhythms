import os
import numpy as np
import wfdb


DATA_DIR = "mit-bih-arrhythmia-database-1.0.0"
BEAT_WINDOW = 500  # in milliseconds
# The fraction of the beat window over which the location of the label
# can be shifted during training to provide augmentation
SHIFT_FRACTION = .5
F_SAMP = 360  # sampling rate in Hz
INCLUDE_IDS = [100, 101, 103, 105, 106, 107, 108, 109, 111, 113]
INCLUDE_CLASSES = {"N": "normal", "L": "lbbb", "/": "paced", "V": "pvc"}

SIGNAL_CACHE = {}


def get_signal(pid):
    if SIGNAL_CACHE.get(pid) is None:
        signal, _ = wfdb.rdsamp(
            os.path.join(DATA_DIR, str(pid)), channels=[0, 1])
        derivative_scale = 10
        d_signal = derivative_scale * (signal[2:, :] - signal[:-2, :]) / 2
        all_signals = np.concatenate((signal[1: -1, :], d_signal), axis=1)
        SIGNAL_CACHE[pid] = all_signals

    return SIGNAL_CACHE[pid]


def get_ann(pid):
    ann = wfdb.rdann(os.path.join(DATA_DIR, str(pid)), "atr")
    return ann.sample, ann.symbol


def count_labels(ids):
    label_count = {}
    for id in ids:
        label_locs, labels = get_ann(id)
        for label in labels:
            try:
                label_count[label] += 1
            except KeyError:
                label_count[label] = 1


def load_waves():
    """
    Pre-load all the data.
    Segment it out into three bins: training, tuning, and testing.
    For now, treat beats as independent. Don't worry about segmenting
    by patient. (Separating patients into training, tuning, and training
    groups would be the absolute most rigorous approach,
    but for the sake of making the example clear, we're simplifying
    it a little.)
    Stratify sampling to make sure that less common classes are adequately
    represented in all groups.
    Undersample the dominant "normal" class, in order to maintain
    class balance.

    Choosing the window width (BEAT_WINDOW) here is important.
    Too narrow and we may miss important signal.
    Too wide and the important signal will get diluted,
    or worse, we'll get parts of adjacent beats which could conflict.

    Using the same size window for every training example ensures
    a good apples-to-apples comparison between them.
    No re-centering will be necessary.
    Because part of the data preparation was identifying
    the peak location accurately, we can be confident that
    the information of interest is centered around each label.
    """
    training_fraction = .6
    tuning_fraction = .2
    testing_fraction = .2

    training_data = []
    tuning_data = []
    testing_data = []

    # Pull all the examples out of all the subjects' records
    examples = []
    for pid in INCLUDE_IDS:
        label_locs, labels = get_ann(pid)
        for i_label, label_loc in enumerate(label_locs):
            label = labels[i_label]
            if label in INCLUDE_CLASSES.keys():
                examples.append((pid, label, label_loc))

    np.random.shuffle(examples)

    # Populate the data sets, one class at a time.

    # Use the count of the "lbbb" class to decide how many examples of each
    # class to include. It's the largest non-normal class.
    class_count = 0
    for example in examples:
        if example[1] == "L":
            class_count += 1

    n_class_training = int(class_count * training_fraction)
    n_class_tuning = int(class_count * tuning_fraction)
    n_class_testing = int(class_count * testing_fraction)
    n_training_repeats = 11

    for label in INCLUDE_CLASSES.keys():
        class_training_data = []
        class_tuning_data = []
        class_testing_data = []

        for example in examples:
            if example[1] == label:
                roll = np.random.sample()
                if roll < training_fraction:
                    class_training_data.append(example)
                elif roll < training_fraction + tuning_fraction:
                    class_tuning_data.append(example)
                else:
                    class_testing_data.append(example)

        i_training_data = np.random.choice(
            np.arange(len(class_training_data), dtype=np.int),
            size=n_class_training)
        for i_data in i_training_data:
            pid, label, label_loc = class_training_data[i_data]
            # Only subject the training data to augmentation
            # int the form of random shifts. The tuning and testing will
            # still focus on the peak beat locations.
            for _ in range(n_training_repeats):
                shift = int(
                    (np.random.sample() - .5)
                    * BEAT_WINDOW
                    * SHIFT_FRACTION)
                example_signal = get_example(
                    pid, label, label_loc, shift=shift)
                if example_signal is not None:
                    training_data.append(
                        (example_signal, INCLUDE_CLASSES[label]))

        i_tuning_data = np.random.choice(
            np.arange(len(class_tuning_data), dtype=np.int),
            size=n_class_tuning)
        for i_data in i_tuning_data:
            pid, label, label_loc = class_tuning_data[i_data]
            example_signal = get_example(pid, label, label_loc)
            if example_signal is not None:
                tuning_data.append(
                    (example_signal, INCLUDE_CLASSES[label]))

        i_testing_data = np.random.choice(
            np.arange(len(class_testing_data), dtype=np.int),
            size=n_class_testing)
        for i_data in i_testing_data:
            pid, label, label_loc = class_testing_data[i_data]
            example_signal = get_example(pid, label, label_loc)
            if example_signal is not None:
                testing_data.append(
                    (example_signal, INCLUDE_CLASSES[label]))

    return training_data, tuning_data, testing_data


def get_example(pid, label, label_loc, shift=0):
    half_sample_window = int(F_SAMP * BEAT_WINDOW / (2 * 1000))  # in samples
    signal = get_signal(pid)
    i_start = label_loc - half_sample_window + shift
    i_end = label_loc + half_sample_window + shift
    sig = signal[i_start: i_end + 1, :].transpose()
    # Check to make sure the full window is supported by the data.
    # At the start and end of the trace, this is not necessarily
    # the case.
    if sig.shape[1] == 2 * half_sample_window + 1:
        return sig
    else:
        return None


def data_generator(examples):
    while True:
        i_data = np.random.choice(len(examples))
        yield examples[i_data]


TRAINING_DATA, TUNING_DATA, TESTING_DATA = load_waves()


# Create the data blocks that Cottonwood will need to build a model
class TrainingData(object):
    def __init__(self):
        self.data = data_generator(TRAINING_DATA)

    def __str__(self):
        return "ECG training data"

    def forward_pass(self, arg):
        return next(self.data)

    def backward_pass(self, arg):
        pass


class TuningData(object):
    def __init__(self):
        self.data = data_generator(TUNING_DATA)

    def __str__(self):
        return "ECG tuning data"

    def forward_pass(self, arg):
        return next(self.data)

    def backward_pass(self, arg):
        pass


class TestingData(object):
    def __init__(self):
        self.data = data_generator(TESTING_DATA)

    def __str__(self):
        return "ECG testing data"

    def forward_pass(self, arg):
        return next(self.data)

    def backward_pass(self, arg):
        pass
